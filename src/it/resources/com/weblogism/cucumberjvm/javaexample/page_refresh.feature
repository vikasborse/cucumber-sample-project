Feature: Page Refresh
  As a user,
  I want to load the page
  So that the dazoodling can be splookified.

  @DEMO-3 @wip
  Scenario: Simple page refreshing
    
    Given I type webpage address in browser
    When I load the page
    Then I should see a greeting

  @DEMO-4 @foo
  Scenario: Not so simple page refreshing
    
    Given I type incorrect webpage address in browser
    When I load the page
    Then I should not see a greeting
    
     @DEMO-4 @foo
  Scenario: Pending scenario
    
    Given I type new webpage address in browser
    When I load the page
    Then I should not see a greeting