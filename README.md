This project helps to understand testing with cucumber.This project starts sample server and tests the same.

Folder main/webapp: contains standard webapp for testing

Folder it: Contains Cucumber features to test the standard webapp


#Install Atlassian SourceTree or any other git tool

open source tree command line tool 


Create a directory for project

Run below commands:

Change directory

cd <Project folder path>

git clone git@bitbucket.org:vikasborse/cucumber-sample-project.git


cd <project folder created >


To compile code:

mvn compile



To execute integration tests:
mvn integration-test


#If you do not have maven installed

#Download and Install Maven and configure the System Variable for Maven

 Visit Maven official website, download the Maven zip file, for example : apache-maven-3.6.0-bin.zip.
 
 Press Windows key, type adva and clicks on the View advanced system settings
 
 In System Properties dialog, select Advanced tab and clicks on the Environment Variables... button.
 
 In “Environment variables” dialog, System variables,  
 
 Add a MAVEN_HOME variable and point it to c:\<Your Folder Path>\apache-maven-3.6.0
 
 In system variables, find PATH, clicks on the Edit... button.Append this ";%MAVEN_HOME%\bin"
 


 
